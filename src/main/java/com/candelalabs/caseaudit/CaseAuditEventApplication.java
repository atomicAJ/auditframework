package com.candelalabs.caseaudit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import com.candelalabs.caseaudit.filter.EventsZuulFilter;
import com.candelalabs.caseaudit.filter.PostFilter;
import com.candelalabs.caseaudit.filter.PreFilter;

@EnableEurekaClient
@SpringBootApplication
@EnableDiscoveryClient
@ImportResource("classpath:camel-route.xml")
public class CaseAuditEventApplication extends SpringBootServletInitializer{

	private static Class<CaseAuditEventApplication> applicationClass = CaseAuditEventApplication.class;
	
	public static void main(String[] args) {

		SpringApplication.run( CaseAuditEventApplication.class , args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
	{
		return application.sources(applicationClass);
	}

	@Bean
	public EventsZuulFilter eventsZuulFilter() {
		return new EventsZuulFilter();
	}
	
	@Bean
    public PreFilter preFilter() {
        return new PreFilter();
    }
    @Bean
    public PostFilter postFilter() {
        return new PostFilter();
    }

}