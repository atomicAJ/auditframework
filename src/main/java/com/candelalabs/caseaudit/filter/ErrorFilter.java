package com.candelalabs.caseaudit.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;

public class ErrorFilter extends ZuulFilter {

	public static final Logger LOGGER = LoggerFactory.getLogger(ErrorFilter.class);
	
	private static final String ERROR = "error";

	@Override
	public String filterType() {
		return ERROR;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		LOGGER.info("Inside Route Filter");
		return null;
	}

}