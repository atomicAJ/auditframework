package com.candelalabs.caseaudit.filter;

import java.io.BufferedReader;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class PreFilter extends ZuulFilter {

	public static final Logger LOGGER = LoggerFactory.getLogger(PreFilter.class);
	
	private static final String PRE = "pre";

	@Override
	public String filterType() {
		return PRE;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		LOGGER.info("Inside pre Filter request {}",request);
		LOGGER.info("{} request to {} PRE ", request.getMethod(),request.getRequestURL());
		LOGGER.info("processName {}",request.getParameter("processName"));
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception e) {
			LOGGER.error("Error in Pre Filter {}",e.getMessage());
		}
		LOGGER.info("Request read at prefilter {} ",jb);
		return null;
	}

}