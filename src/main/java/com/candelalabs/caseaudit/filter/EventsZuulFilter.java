package com.candelalabs.caseaudit.filter;

import com.netflix.zuul.context.RequestContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;

@Component
public class EventsZuulFilter extends ZuulFilter {

	public static final Logger LOGGER = LoggerFactory.getLogger(EventsZuulFilter.class);


	@Override
	public String filterType() {
		return "route";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public boolean shouldFilter() {
		return RequestContext.getCurrentContext().getRouteHost() != null
				&& RequestContext.getCurrentContext().sendZuulResponse();
	}

	@Override
	public Object run() {
		System.out.println("Entered the filter Router");		 
		return null;
	}
}
