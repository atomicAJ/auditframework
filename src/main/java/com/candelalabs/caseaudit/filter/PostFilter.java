package com.candelalabs.caseaudit.filter;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.util.HttpURLConnection;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import com.candelalabs.caseaudit.util.CaseAuditUtilServices;
import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

public class PostFilter extends ZuulFilter {

	public static final Logger LOGGER = LoggerFactory.getLogger(EventsZuulFilter.class);

	private static final String EVENT = "event";
	private static final String REQUESTURI = "requestURI";
	private static final String BACKSLASH = "/";
	private static final String UTF = "UTF-8";

	HttpURLConnection connection = null;
	String body;

	private static final String POST = "post";

	@Autowired
	private CaseAuditUtilServices utilService;

	@Value(value = "${event.AuditUrl:}")
	private String eventURL;

	@Override
	public String filterType() {
		return POST;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {

		LOGGER.info("Inside Response Filter");
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest request = context.getRequest();
		if (context.getResponseStatusCode() == HttpStatus.SC_OK) {
			InputStream inputStream = null;
			JSONObject requestData = null;
			try {
				inputStream = request.getInputStream();
			} catch (IOException e1) {
				LOGGER.error("Request requestBody  inputStream+++  ", e1);
			}
			try {
				body = StreamUtils.copyToString(inputStream, Charset.forName(UTF));
				requestData = new JSONObject(body);
				if (null != context.get(REQUESTURI)) {
					String event = ((context.get(REQUESTURI)).toString()).replaceAll(BACKSLASH, "");
					requestData.put(EVENT, utilService.getEventType(event));
				}
			} catch (Exception e) {
				LOGGER.error("Exception in Zuul", e);
			}
			try {
				RestTemplate restTemplate = new RestTemplate();
				String responseCode = restTemplate.postForObject(eventURL, requestData.toString(), String.class);
				LOGGER.info("Sent to event URL : {}", responseCode);
			} catch (Exception e) {
				LOGGER.error("Inside Route Fileter", e);
			}
		} else
			LOGGER.error("Audit will not be captured as the event call response code is:{}",
					context.getResponseStatusCode());

		return null;
	}

}