package com.candelalabs.caseaudit.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.candelalabs.caseaudit.util.CaseAuditUtilServices;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RefreshScope
@RestController
public class EventsRestController {

	@Autowired
	private CaseAuditUtilServices utilService;

	@Autowired
	ProducerTemplate producerTemplate;

	Map<String, Object> resultMap;

	private static final Logger LOGGER = LoggerFactory.getLogger(EventsRestController.class);
	private static final String EVENT = "event";

	ObjectMapper objectMapper = new ObjectMapper();

	@PostMapping(path = "/logevent")
	public void sendEventToKafka(@RequestBody String eventJson) throws IOException {

		LOGGER.info("****** Entered Audit MicroService ******* {}", eventJson);
		try {
			Map<String, Object> map = objectMapper.readValue(eventJson, new TypeReference<Map<String, Object>>() {
			});
			if (map.get(EVENT).toString().contains("/")) {
				LOGGER.info("Event captured from gateway");
				map.put(EVENT, utilService.getEventType((String) map.get(EVENT)));
			}
			String event = (String) map.get(EVENT);
			if (event != null) {
				resultMap = utilService.getEventFields(event, map);
				LOGGER.debug(" @@@@@@@@@@  Event Data to be audited  @@@@@@@@@ {}", resultMap);
				JSONObject data = new JSONObject(resultMap);
				utilService.mapAuditData(data);
			} else
				LOGGER.info("Audit Event is : {}", event);
		} catch (Exception e) {
			LOGGER.info("Error seen in audit controller", e);
		}

	}

}
