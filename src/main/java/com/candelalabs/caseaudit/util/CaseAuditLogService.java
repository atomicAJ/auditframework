package com.candelalabs.caseaudit.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.candelalabs.caseaudit.model.CaseAuditFilter;
import com.candelalabs.caseaudit.model.CaseAuditRequestBean;

public class CaseAuditLogService {

	public static final Logger LOGGER = LoggerFactory.getLogger(CaseAuditLogService.class);

	@Before(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(caseAudit)")
	public void beforeAdvice(JoinPoint joinPoint, String caseAudit) {
		LOGGER.debug(CaseAuditConstants.BEFORE + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + caseAudit);
	}

	@After(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(caseAudit)")

	public void afterAdvice(JoinPoint joinPoint, String caseAudit) {
		LOGGER.debug(CaseAuditConstants.AFTER + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLED + caseAudit);
	}

	@Before(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(caseAudit)")
	public void beforeAdvice(JoinPoint joinPoint, CaseAuditRequestBean caseAudit) {
		LOGGER.debug(CaseAuditConstants.BEFORE + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + caseAudit);
	}

	@After(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(caseAudit)")

	public void afterAdvice(JoinPoint joinPoint, CaseAuditRequestBean caseAudit) {
		LOGGER.debug(CaseAuditConstants.AFTER + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLED + caseAudit);
	}

	@Before(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(filter,caseId)")
	public void beforeAdvice(JoinPoint joinPoint, CaseAuditFilter filter, String caseId) {
		LOGGER.debug(CaseAuditConstants.BEFORE + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + filter + CaseAuditConstants.CASEID + caseId);
	}

	@After(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(filter,caseId)")

	public void afterAdvice(JoinPoint joinPoint, CaseAuditFilter filter, String caseId) {
		LOGGER.debug(CaseAuditConstants.AFTER + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + filter + CaseAuditConstants.CASEID + caseId);
	}

	@Before(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(elasticSearchURL,processId,query)")
	public void beforeAdvice(JoinPoint joinPoint, String elasticSearchURL, String processId, String query) {
		LOGGER.debug(CaseAuditConstants.BEFORE + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + elasticSearchURL + CaseAuditConstants.CASEID + processId + CaseAuditConstants.QUERY + query);
	}

	@After(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(elasticSearchURL,processId,query)")

	public void afterAdvice(JoinPoint joinPoint, String elasticSearchURL, String processId, String query) {
		LOGGER.debug(CaseAuditConstants.AFTER + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + elasticSearchURL + CaseAuditConstants.CASEID + processId + CaseAuditConstants.QUERY + query);
	}

	@Before(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(elasticSearchURL,processId,query)")
	public void beforeAdvice(JoinPoint joinPoint, String field, String value) {
		LOGGER.debug(CaseAuditConstants.BEFORE + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + field + CaseAuditConstants.VALUE + value);
	}

	@After(value = "execution(* com.candelalabs.caseaudit.CaseAuditServiceProvider.*(..)) and args(elasticSearchURL,processId,query)")

	public void afterAdvice(JoinPoint joinPoint, String field, String value) {
		LOGGER.debug(CaseAuditConstants.AFTER + joinPoint.getSignature());
		LOGGER.debug(CaseAuditConstants.CALLING + field + CaseAuditConstants.VALUE + value);
	}

}
