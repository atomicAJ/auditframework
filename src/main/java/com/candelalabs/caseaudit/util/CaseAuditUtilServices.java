package com.candelalabs.caseaudit.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.camel.ProducerTemplate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.candelalabs.caseaudit.controller.EventsRestController;
import com.candelalabs.caseaudit.model.CaseAuditRequestBean;
import com.candelalabs.caseaudit.respository.CaseAuditDBService;
import net.minidev.json.parser.JSONParser;

@ConfigurationProperties(prefix = "candela.events")
@Service
public class CaseAuditUtilServices {

	@Autowired
	private CaseAuditDBService caseAuditDBService;

	@Autowired
	ProducerTemplate producerTemplate;

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Event related fields to be fetched from request, defined in properties file
	 **/
	private List<String> auditconstants;

	private String tPIDsFormat;

	private Map<String, List<String>> fields;

	/**
	 * @return the fields
	 */
	public Map<String, List<String>> getFields() {
		return fields;
	}

	/**
	 * @param fields the fields to set
	 */
	public void setFields(Map<String, List<String>> fields) {
		this.fields = fields;
	}

	private static final String EVENT = "event";
	private static final String ASSIGNEVNT = "assign";
	private static final String FROMUSER = "fromUser";
	private static final String COMMA = ",";
	private static final String AUDITEVENT = "auditEvent";
	private static final String CASEAUDITINSERTIONTIME = "caseAuditInsertionTime";
	private static final String CAMEL = "direct:camel";
	private static final String PARSEERROR = "Error while parsiong the data {}";
	private static final String OPENFLOWER = "{";
	private static final String CLOSEFLOWER = "}";
	private static final String OPENSQUARE = "[";
	private static final String CLOSESQUARE = "]";
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	private static final String EQUALS = "=";

	@Value(value = "${event.actions}")
	String eventNames;

	@Value(value = "${event.actions.values}")
	String eventValues;

	@Value(value = "${notification.restUrl}")
	private String url;

	private static final String CASEID = "caseId";
	private static final String CASEINFO = "caseinfo";
	private static final String SPILTON = "\\s*::\\s*";
	private static final String TPIDS = "taskPIDs";
	private static final String TPID = "taskPid";

	private static final Logger LOGGER = LoggerFactory.getLogger(EventsRestController.class);

	public List<String> getAuditconstants() {
		return auditconstants;
	}

	public void setAuditconstants(List<String> auditconstants) {
		this.auditconstants = auditconstants;
	}

	public String gettPIDsFormat() {
		return tPIDsFormat;
	}

	public void settPIDsFormat(String tPIDsFormat) {
		this.tPIDsFormat = tPIDsFormat;
	}

	/** Method to get Audit message based on the audit action/task **/
	public String getEventMessage(CaseAuditRequestBean caseAuditData) {
		switch (caseAuditData.getEvent()) {
		case CaseAuditConstants.ASSIGN: {
			HashMap<String, String> actionParameters = (HashMap<String, String>) caseAuditData.getEventDetails();
			return String.format(CaseAuditConstants.ASSIGN, actionParameters.get(CaseAuditConstants.FROMUSER),
					actionParameters.get(CaseAuditConstants.TOUSER));
		}
		case CaseAuditConstants.DECISION: {
			HashMap<String, String> actionParameters = (HashMap<String, String>) caseAuditData.getEventDetails();
			return String.format(CaseAuditConstants.DECISIONMSG, actionParameters.get(CaseAuditConstants.DECISIONNAME));
		}
		default:
			return null;
		}
	}

	/** Method to get Audit fields based on action **/
	public Map<String, Object> getEventFields(String event, Map<String, Object> map) {
		Map<String, Object> resultMap = null;
		if (getFields().get(event) != null) {
			resultMap = getFields().get(event).stream().filter(map::containsKey)
					.collect(Collectors.toMap(Function.identity(), map::get));
		}
		return resultMap;
	}

	/** Method to forward map request data **/
	public List<String> mapAuditData(JSONObject json) {
		List<String> auditData = new ArrayList<>();
		json.put(CASEAUDITINSERTIONTIME, new Timestamp(System.currentTimeMillis()).toString());
		if (json.has(TPIDS)) {
			json.put(AUDITEVENT, TRUE);
			mapTaskPIDswithoutSpilt(json);
		} else {
			if (json.has(CASEID)) {
				json = new JSONObject(addCaseInfo(json.toString()));
			}
			json.put(AUDITEVENT, FALSE);
			producerTemplate.sendBody("direct:kafka", json.toString());
		}
		return auditData;
	}

	/** Method to transform request JSON to case Audit JSON using JOLT **/
	public void mapTaskPIDs(JSONObject json) {
		JSONArray taskDetails = json.getJSONArray(TPIDS);
		json.remove(TPIDS);
		List<String> fields = Arrays.asList(gettPIDsFormat().split(SPILTON));
		try {
			for (int i = 0; i < taskDetails.length(); i++) {
				List<String> values = Arrays.asList(((taskDetails.get(i)).toString()).split(SPILTON));
				if (!values.isEmpty() && !fields.isEmpty() && values.size() == fields.size()) {
					for (int j = 0; j < fields.size(); j++) {
						if (fields.get(j).equals(CASEID)) {
							json.put(CASEINFO, new JSONObject(caseAuditDBService.fetchCaseData(values.get(j))));
						}
						json.put(fields.get(j), values.get(j));
					}
				}
				producerTemplate.sendBody(CAMEL, json.toString());
			}
		} catch (Exception e) {
			LOGGER.error(PARSEERROR, e.getMessage());
		}
	}

	/** Method to get Case info based on caseId **/
	public String addCaseInfo(String json) {
		JSONObject data = new JSONObject(json);
		data.put(CASEINFO, new JSONObject(caseAuditDBService.fetchCaseData(data.get(CASEID).toString())));
		return data.toString();

	}

	/** Method to Notify the user **/
	public void notifyUser(String json) {
		try {
			JSONParser parser = new JSONParser(JSONParser.MODE_PERMISSIVE);
			net.minidev.json.JSONObject data = (net.minidev.json.JSONObject) parser.parse(json);
			restTemplate.postForObject(url, data, String.class);
			LOGGER.info("Notification sent successfully for event {}", data.get(EVENT));
		} catch (Exception e) {
			LOGGER.error("Error while sending Notification {}", e.getMessage());
		}
	}

	/**
	 * Method to transform request JSON Array to separate individual JSON using JOLT
	 **/
	public void mapTaskPIDswithoutSpilt(JSONObject json) {
		JSONArray taskDetails = json.getJSONArray(TPIDS);
		json.remove(TPIDS);
		if (json.has(FROMUSER)) {
			JSONArray fromUsers = json.getJSONArray(FROMUSER);
			json.remove(FROMUSER);
			try {
				for (int i = 0; i < taskDetails.length(); i++) {
					json.put(TPIDS, taskDetails.get(i));
					json.put(FROMUSER, fromUsers.get(i));
					producerTemplate.sendBody(CAMEL, json.toString());
				}
			} catch (Exception e) {
				LOGGER.error(PARSEERROR, e.getMessage());
			}
		} else {
			try {
				for (int i = 0; i < taskDetails.length(); i++) {
					json.put(TPIDS, taskDetails.get(i));
					producerTemplate.sendBody(CAMEL, json.toString());
				}
			} catch (Exception e) {
				LOGGER.error(PARSEERROR, e.getMessage());
			}

		}
	}

	/** Method to map event Type to be appended to JSON **/
	public String getEventType(String data) {
		String[] actions = eventNames.split(COMMA);
		String[] actionValues = eventValues.split(COMMA);
		if (null != actions && null != actionValues && actions.length == actionValues.length) {
			for (int i = 0; i < actions.length; i++) {
				if (actions[i].matches(data)) {
					return actionValues[i];
				}
			}
		} else {
			LOGGER.error("Event Actions and Event Actions values incorrectly configured in properties file");
		}
		return data;
	}

	/** Method to check if taskHeader to be updated or not **/
	public boolean returnCountData(String data) {
		if (null != data && data != "") {
			data = data.replace(OPENFLOWER, "");
			data = data.replace(OPENSQUARE, "");
			data = data.replace(CLOSESQUARE, "");
			data = data.replace(CLOSEFLOWER, "");
			LOGGER.info("The count maching the task id is : {}", data);
			String[] counts = data.split(EQUALS);
			if (counts.length > 1 && Integer.valueOf(counts[1]) == 0) {
				return true;
			}
		}
		return false;
	}
}
