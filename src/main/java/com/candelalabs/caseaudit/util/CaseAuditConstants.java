package com.candelalabs.caseaudit.util;

public class CaseAuditConstants {

	/** Events constants **/
	public static final String ASSIGN = "assign";
	public static final String CHECKOUT = "checkout";	
	public static final String DECISION = "decision";
	public static final String PRIORITY = "priority";
	public static final String REASSIGNTOGROUP = "reassignToGroup";
	public static final String REASSIGNTOUSER = "reassignToUser";
	public static final String CASEINITIATIONSUCCESS = "caseInitiationSuccess";
	public static final String NEWDOCUMENT = "newdocument";
	public static final String SAVECASESHEET = "saveCasesheet";
	
	/** Event messages constants **/
	public static final String ASSIGNMSG = "Case assigned from %1$s to %2$s";
	public static final String CHECKOUTMSG = "Case has been cheked out";	
	public static final String DECISIONMSG = "Decision taken: %1$s";
	
	/** Task Details variables constants **/
	public static final String FROMUSER="fromUser";
	public static final String TOUSER="toUser";
	public static final String DECISIONNAME="decisionName";
	
	public static final String BEFORE = "Before method:";
	public static final String AFTER = "After method:";
	public static final String CALLING = "Calling service with - ";
	public static final String CALLED = "Successfully called with - ";
	public static final String CASEID = " CaseId: ";
	public static final String QUERY = " Query: ";
	public static final String VALUE = " value: ";

}
