
package com.candelalabs.caseaudit.configuration;

import java.sql.Driver;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.web.client.RestTemplate;

@Configuration
public class DatasourceConfiguration {

	@Value(value = "${spring.datasource.url}")
	String url;

	@Value(value = "${spring.datasource.username}")
	String uname;

	@Value(value = "${spring.datasource.password}")
	String password;

	@Value(value = "${spring.datasource.driver-class-name}")
	String driver;

	@Bean

	@Primary
	public DataSource hsqldbDataSource() throws SQLException {
		final SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		try {
			dataSource.setDriver((Driver) Class.forName(driver).newInstance());
		dataSource.setUrl(url);
		dataSource.setUsername(uname);
		dataSource.setPassword(password);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataSource;
	}
	
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate()
	{
		return new RestTemplate();
	}
}
