package com.candelalabs.caseaudit.model;

import java.util.Map;

public class CaseAuditRequestBean {

	private Long taskId;
	
	private Long caseId;

	private String user;

	private String processName;

	private String processInstanceName;

	private String activityName;

	private String caseAuditInsertionTime;

	private Map<String, Object> caseinfo;

	private String event;

	private Map<String, String> eventDetails;

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessInstanceName() {
		return processInstanceName;
	}

	public void setProcessInstanceName(String processInstanceName) {
		this.processInstanceName = processInstanceName;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getCaseAuditInsertionTime() {
		return caseAuditInsertionTime;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Map<String, String> getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(Map<String, String> eventDetails) {
		this.eventDetails = eventDetails;
	}

	public void setCaseAuditInsertionTime(String caseAuditInsertionTime) {
		this.caseAuditInsertionTime = caseAuditInsertionTime;
	}

	public Map<String, Object> getCaseinfo() {
		return caseinfo;
	}

	public void setCaseinfo(Map<String, Object> caseinfo) {
		this.caseinfo = caseinfo;
	}

}
