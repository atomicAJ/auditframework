package com.candelalabs.caseaudit.respository;

import com.candelalabs.caseaudit.model.CaseAuditRequestBean;

public interface CaseAuditDBService {

	public String saveCaseAuditData(CaseAuditRequestBean caseAudit);
	
	public String fetchCaseData(String caseId);

}
