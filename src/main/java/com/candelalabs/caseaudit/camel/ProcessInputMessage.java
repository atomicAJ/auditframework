package com.candelalabs.caseaudit.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessInputMessage implements Processor
{
	private static final Logger log = LoggerFactory.getLogger(ProcessInputMessage.class);

	@Override
	public void process(Exchange exchange) throws Exception
	{
		log.info("Body is {}", exchange.getIn().getBody(String.class));
	}	
}
